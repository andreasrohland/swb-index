---
title: "Model Specification and Estimation"
author: "Andreas Rohland"
date: "31.05.2020"
output: html_notebook
---

# Load packages and initiliase

```{r setup, include=FALSE}
library(haven)
library(tidyverse)
library(brms)
library(bayesplot)
library(shinystan)
library(sjlabelled)
library(parallel)
options(mc.cores = detectCores()) 

Sys.setenv(LANG="en")
par(ask=F)
```

# Load Datasets
Load the datasets generated with stata and prepared in 13

```{r}
#Load poststratification table
pstable <- readRDS("..//data/pstable_kkz_prep.Rds")

#load SOEP data
soep <- readRDS("..//data/soep_kkz_prep.Rds")

#load SOEP sample
sample <- readRDS("..//data/soep_sample.Rds")
```

# Model fit1 with fixed effects

```{r}
get_prior(lsat ~ age.sex + educ + maritalstatus + job + german + qkkz, data = sample, family = gaussian)

prior1 <- c(prior(normal(6,1), class = Intercept),
              prior(normal(0,0.75), class = b),
              prior(exponential(0.5), class = sigma))
fit1 <- brm(
  formula = lsat ~ age.sex + educ + maritalstatus + job + german + qkkz,
  data = sample, family = gaussian,
  control=list(adapt_delta= 0.95, max_treedepth=15),
  chains = 8, cores = 8, iter = 2000, warmup = 1000, file = "../models/fit1_fixed",
  prior = prior1)
fit1 <- add_criterion(fit1, "waic")
```

# Model 2 with random effects

```{r}
get_prior(formula = lsat ~ (1| age.sex) + (1|educ) + (1|maritalstatus) + (1|job) + (1|german) + (1 |qkkz), data = sample)

prior2 <- c(prior(normal(6,1), class = Intercept),
              prior(normal(0,0.75), class = sd),
              prior(exponential(0.5), class = sigma))

fit2 <- brm(
  formula = lsat ~ (1| age.sex) + (1|educ) + (1|maritalstatus) + (1|job) + (1|german) + (1 |qkkz),
  data = sample, family = gaussian,
  control=list(adapt_delta=.95, max_treedepth=15),
  chains = 8, cores = 8, iter = 2000, warmup = 1000, file = "../models/fit2_ranef",
  prior = prior2)
fit2 <- add_criterion(fit2, "waic")

```

# Model 3 with varying slopes at regional level

```{r} 
get_prior(formula = lsat ~  (1 + age.sex + educ + maritalstatus + job + german | qkkz), data = sample)

prior3 <- c(prior(normal(6,1), class = Intercept),
              prior(normal(0,0.75), class = sd),
              prior(exponential(0.5), class = sigma),
              prior(lkj(1), class = cor))

fit3 <- brm(
  formula = lsat ~  (1 + age.sex + educ + maritalstatus + job + german | qkkz),
  data = sample, family = gaussian,
  control=list(adapt_delta=.99, max_treedepth=15),
  chains = 8, cores = 8, iter = 2000, warmup = 1000, file = "../models/fit3_varying_ranef",
  prior = prior3)
fit3 <- add_criterion(fit3, "waic")
```
# Model 4 with fixed effects and varying slopes

```{r} 
get_prior(formula = lsat ~ female + agegrp + educ + maritalstatus + job + german + (1 + female + agegrp + educ + maritalstatus + job + german | qkkz), 
          data = sample, family = gaussian)

prior4 <- c(prior(normal(6,1), class = Intercept),
            prior(normal(0,0.75), class = b),
            prior(normal(0,0.75), class = sd),
            prior(exponential(0.5), class = sigma),
            prior(lkj(1), class = cor))

fit4 <- brm(
  formula = lsat ~ female + agegrp + educ + maritalstatus + job + german + (1 + female + agegrp + educ + maritalstatus + job + german | qkkz),
  data = sample, family = gaussian,
  control=list(adapt_delta=.99, max_treedepth=15),
  chains = 8, cores = 8, iter = 2000, warmup = 1000, file = "../models/fit4_fixef_ranef",
  prior = prior4)

fit4 <- add_criterion(fit4, "waic")
```

# Model 5 with fixed and varying effects by region and regioneffects

```{r} 
get_prior(formula = lsat ~ female + agegrp + educ + maritalstatus + job + german + unemployment + income + (1 + female + agegrp + educ + maritalstatus + job + german | qkkz), 
          data = sample, family = gaussian)

prior4 <- c(prior(normal(6,1), class = Intercept),
            prior(normal(0,0.75), class = b),
            prior(normal(0,0.75), class = sd),
            prior(exponential(0.5), class = sigma),
            prior(lkj(1), class = cor))

fit5_full <- brm(
  formula = lsat ~ female + agegrp + educ + maritalstatus + job + german + unemployment + income + (1 + female + agegrp + educ + maritalstatus + job + german | qkkz),
  data = soep, family = gaussian,
  control=list(adapt_delta=.99, max_treedepth=15),
  chains = 8, cores = 8, iter = 4000, warmup = 2000, file = "../models/fit5_fixef_ranef_full",
  prior = prior4)

fit5_full <- add_criterion(fit5_full, "waic")
```

# Check the MCMC performance
fit1 encountered some divergent transitions

```{r}

print(fit1)
print(fit2)
print(fit3)
print(fit4)
print(fit5)

  
loo_compare(fit1, fit2, fit3, fit4, fit5, criterion = "waic") %>% 
  print(simplify=F)
round(model_weights(fit1, fit2, fit3, fit4, fit5, criterion = "waic"), 3)

plot(fit1)
plot(fit3)
plot(fit7)
plot(fit8)

mcmc_plot(object = fit1)
stanplot(fit2)
stanplot(fit3)
stanplot(fit4)

coef(fit9)
ranef(fit9)
fixef(fit9)

launch_shinystan(fit5)
```
