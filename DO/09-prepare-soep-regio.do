/* ************************************************************************* * 
* 	Creator: Andreas Rohland
* 	Project: MRP for regional SWB index
* 	Last modified: 2020-07-16
*  ------------------------------------------------------------------------  * 
*	This file prepares the SOEP KreisKennZiffern for merging with MZKR.
** ************************************************************************* */ 
clear all
set more off

/*Import the KKZ for 2000 to 2013
import excel "/home/hiwi/Documents/swb-index/data/soep_regio/ReferenzschluÌssel Kreise von 1990 bis 2013.xls", sheet("2000-2013") firstrow clear
gen qkkz = Kreise31122000/1000
*/

*Import KreisKennZiffern for 2000 only
import delimited "/home/hiwi/Documents/swb-index/data/soep_regio/kreise.txt", delimiter(";") varnames(1) clear 
rename kreiskennziffer qkkz
save "${path_data}/kreisnamen2000.dta" ,replace

*Import the SOEP Regiofile-KKZ
use "${path_data}/soep_regio/soep_kkz2000.dta", clear

*drop missing values foe SOEP
drop if qkkz < 0

*Include if using the xls file with 2013 data
replace qkkz = 11000 if qkkz == 11100 | qkkz == 11200

merge m:m qkkz using "${path_data}/kreisnamen2000.dta"

keep if _merge ==3
drop _merge
save "${path_data}/kkz_code.dta", replace

/* The following are not matched.
 Kreisname
Baden-Baden
Weiden i.d.OPf., Stadt
Memmingen, Stadt
Mecklenburgische Seenplatte
Anhalt-Bitterfeld
Eisenach, Stadt
