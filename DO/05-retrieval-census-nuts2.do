/* ************************************************************************* * 
* 	Creator: Andreas
* 	Project: MRP for regional SWB index
* 	Last modified: 2020-05-23
*  ------------------------------------------------------------------------  * 
*	This file creates the poststratification table from census data (CENSUS HUB) https://ec.europa.eu/CensusHub2/intermediate.do?&method=forwardResult.
*	Cross sectional data set for 2011 (census year)
** ************************************************************************* */ 

import delimited "${path_data}/Nuts eurostat/nuts2_age_sex_edu.csv", clear 

drop yae fïhnchen fussnoten time cas coc
drop if age == "Y_LT15" //Under 15 year olds, don't appear in SOEP
drop if edu == "UNK" | edu == "NONE" | edu == "NAP" // No obs for these categories

* Generate female dummy
gen female = 0
replace female = 1 if sex == "F"
label var female "Female"

*Get "N" the count for that cell
rename wert N
label var N "Count in Census"

*Generate age 5 categories 
gen agegrp = .
replace agegrp = 1 if age == "Y15-29"
replace agegrp = 2 if age == "Y30-49"
replace agegrp = 3 if age == "Y50-64"
replace agegrp = 4 if age == "Y65-84"
replace agegrp = 5 if age == "Y_GE85"
label var agegrp "Age group"

label define agegrp ///
	1 "15-29" ///
	2 "30-49" ///
	3 "50-64" ///
	4 "65-84" ///
	5 ">85" 
label value agegrp agegrp
	
/*Generate 6 Educ categories following ISCED classification (SOEP has 8 Categories)
      ISCED-Ebene 1: Primarbereich
      ISCED-Ebene 2: Sekundarbereich I
      ISCED-Ebene 3: Sekundarbereich II
      ISCED-Ebene 4: Nichttertiäre Bildung nach dem Sekundarbereich
      ISCED-Ebene 5: Erste Stufe der tertiären Bildung
      ISCED-Ebene 6: Zweite Stufe der tertiären Bildung
*/
gen educ = .
replace educ = 1 if edu == "ED1"
replace educ = 2 if edu == "ED2"
replace educ = 3 if edu == "ED3"
replace educ = 4 if edu == "ED4"
replace educ = 5 if edu == "ED5"
replace educ = 6 if edu == "ED6"

label var educ "Education ISCED"
label define educ ///
	1 "ISCED-Ebene 1: Primarbereich" ///
	2 "ISCED-Ebene 2: Sekundarbereich I" ///
	3 "ISCED-Ebene 3: Sekundarbereich II" ///
	4 "ISCED-Ebene 4: Nichttertiäre Bildung nach dem Sekundarbereich" ///
	5 "ISCED-Ebene 5: Erste Stufe der tertiären Bildung" ///
	6 "ISCED-Ebene 6: Zweite Stufe der tertiären Bildung" 
label value educ educ

*Generate GEO categories for Nuts2 regions mapping to the numbers in SOEP regio

gen nuts2 = .
replace nuts2 = 1 if geo == "DE11"
replace nuts2 = 2 if geo == "DE12"
replace nuts2 = 3 if geo == "DE13"
replace nuts2 = 4 if geo == "DE14"
replace nuts2 = 5 if geo == "DE21"
replace nuts2 = 6 if geo == "DE22"
replace nuts2 = 7 if geo == "DE23"
replace nuts2 = 8 if geo == "DE24"
replace nuts2 = 9 if geo == "DE25"
replace nuts2 = 10 if geo == "DE26"
replace nuts2 = 11 if geo == "DE27"
replace nuts2 = 12 if geo == "DE30"
replace nuts2 = 13 if geo == "DE40" //skip 14 because 2 Brandenburg regions got merged
replace nuts2 = 15 if geo == "DE50"
replace nuts2 = 16 if geo == "DE60"
replace nuts2 = 17 if geo == "DE71"
replace nuts2 = 18 if geo == "DE72"
replace nuts2 = 19 if geo == "DE73"
replace nuts2 = 20 if geo == "DE80"
replace nuts2 = 21 if geo == "DE91"
replace nuts2 = 22 if geo == "DE92"
replace nuts2 = 23 if geo == "DE93"
replace nuts2 = 24 if geo == "DE94"
replace nuts2 = 25 if geo == "DEA1"
replace nuts2 = 26 if geo == "DEA2"
replace nuts2 = 27 if geo == "DEA3"
replace nuts2 = 28 if geo == "DEA4"
replace nuts2 = 29 if geo == "DEA5"
replace nuts2 = 30 if geo == "DEB1"
replace nuts2 = 31 if geo == "DEB2"
replace nuts2 = 32 if geo == "DEB3"
replace nuts2 = 33 if geo == "DEC0"
replace nuts2 = 34 if geo == "DED4" // Leipzig is DED5 instead of DED3 in this file 
replace nuts2 = 35 if geo == "DED2"
replace nuts2 = 36 if geo == "DED5" // Chemnitz is DED4 instead of DED1
replace nuts2 = 37 if geo == "DEE0"
replace nuts2 = 38 if geo == "DEF0"
replace nuts2 = 39 if geo == "DEG0"

label var nuts2 "NUTS2 Region"
label def nuts2                          ///
        1 "[DE11] Stuttgart"         ///
        2 "[DE12] Karlsruhe"          ///
        3 "[DE13] Freiburg"         ///
        4 "[DE14] Tuebingen"          ///
		5 "[DE21] Oberbayern"         ///
        6 "[DE22] Niederbayern"          ///
		7 "[DE23] Oberpfalz"         ///
        8 "[DE24] Oberfranken"          ///
		9 "[DE25] Mittelfranken"         ///
        10 "[DE26] Unterfranken"          ///
		11 "[DE27] Schwaben"         ///
        12 "[DE30] Berlin"          ///
		13 "[DE40] Brandenburg"         ///
        14 "Old Brandenburg Süd"          ///
		15 "[DE50] Bremen"         ///
        16 "[DE60] Hamburg"          ///
		17 "[DE71] Darmstadt"         ///
        18 "[DE72] Giessen"          ///
		19 "[DE73] Kassel"         ///
        20 "[DE80] Mecklenburg-Vorpommern"         ///
        21 "[DE91] Braunschweig"         ///
        22 "[DE92] Hannover"          ///
        23 "[DE93] Lueneburg"         ///
        24 "[DE94] Weser-Ems"          ///
		25 "[DEA1] Duesseldorf"         ///
        26 "[DEA2] Koeln"          ///
		27 "[DEA3] Muenster"         ///
        28 "[DEA4] Detmold"          ///
		29 "[DEA5] Arnsberg"         ///
        30 "[DEB1] Koblenz"          ///
		31 "[DEB2] Trier"         ///
        32 "[DEB3] Rheinhessen-Pfalz"          ///
        33 "[DEC0] Saarland"         ///
        34 "[DED4] Chemnitz"          ///
		35 "[DED2] Dresden"         ///
        36 "[DED5] Leipzig"          ///
		37 "[DEE0] Sachsen-Anhalt"         ///
        38 "[DEF0] Schleswig-Holstein"          ///
		39 "[DEG0] Thueringen"         
label value nuts2 nuts2  

drop geo sex edu age
order nuts female agegrp educ N

saveold "${path_data}/nuts2/census_nuts2.dta", replace version(12)
exit
