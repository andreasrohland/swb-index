/* ************************************************************************* * 
* 	Creator: Andreas
* 	Project: MRP for regional SWB index
* 	Last modified: 2020-05-11
*  ------------------------------------------------------------------------  * 
*	This file retrieves data from SOEP v34 1984-2016.
*	Cross sectional data set for 2011 (census year)
** ************************************************************************* */ 

/* ************************************************************************* * 
*	COLLECT INDIVIDUAL CHARACTERISTICS FROM PPATHL
** ************************************************************************* */ 

/* ************************************************************************* * 
	PPFADL
	------
	cid             long    %12.0g                Case ID, Ursprungshaushaltsnummer
	pid             long   %12.0g                 Unveraenderliche Personennummer
	syear           int    %8.0g                  Erhebungsjahr (SurveyYear)
	sex             byte   %53.0g      sex        Geschlecht
	gebjahr         int    %8.0g                  Geburtsjahr -4Steller-
	migback         byte   %53.0g      migback    Migrationshintergrund
	hid             long   %12.0g                 ID Haushalt
	nett1           byte   %53.0g      nett1      Aktueller Befragungsstatus (Netold)
	pop             byte   %53.0g      pop        Aktuelle Populationszugehoerigkeit
	phrf            double %10.0g                 Hochrechnungsfaktor
** ************************************************************************* */ 

use hid cid pid syear 													///
	gebjahr sex nett1 pop phrf 						///
	if syear==2011 													///
	using "${path_soep}/ppathl.dta", clear
sort pid syear
keep if nett1==1
keep if pop==1 | pop==2
gen age = syear - gebjahr if gebjahr>0
label var age "Age"

gen agegrp = .
replace agegrp = 1 if age>=15 & age<30
replace agegrp = 2 if age>=30 & age<50
replace agegrp = 3 if age>=50 & age<65
replace agegrp = 4 if age>=65 & age<85
replace agegrp = 5 if age>=85 & age<.
label var agegrp "Age group"

label define agegrp ///
	1 "15-29" ///
	2 "30-49" ///
	3 "50-64" ///
	4 "65-84" ///
	5 ">85" 
label value agegrp agegrp

recode sex (1=0) (2=1)  (else=.), gen(female)
label var female "Female (0/1)"
drop sex

keep hid cid pid syear agegrp female phrf
save "${path_tmp}/ppathl.tmp.dta", replace	

/* ************************************************************************* * 
*	COLLECT BULA INFORMATION FROM PPATHL
** ************************************************************************* */ 
/*	--------( pull: hbrutto )--------------------------------------------- **
	bula            byte   %22.0g      bula       Bundesland
** ------------------------------------------------------------------------ */ 
use hid syear bula ///
	if syear==2011 ///
	using "${path_soep}/hbrutto.dta", clear
mvdecode _all, mv(-3=.a \ -2=.b \ -1=.c)
sort hid syear
label var bula "State"
label def bula  ///
			1 "SH"       ///
			2 "HH"       ///
			3 "NI"       ///
			4 "HB"       ///
			5 "NRW"       ///
			6 "HE"       ///
			7 "RP"       ///
			8 "BW"       ///
			9 "BY"       ///
			10 "SL"     ///
			11 "BE"      ///
			12 "BB"      ///
			13 "MV"      ///
			14 "SN"      ///
			15 "ST"      ///
			16 "TH", modify
label val bula bula

save "${path_tmp}/hbrutto.tmp.dta", replace

/* ************************************************************************* * 
*	COLLECT REGIONAL INFORMATION FROM ROR_L
** ************************************************************************* */ 
/*	--------( pull: ror_l)--------------------------------------------- **
hhnr            long    %12.0g                Ursprungshaushaltsnummer
hhnrakt         long    %12.0g                Aktuelle Haushaltsnummer
year            int     %8.0g                 Erhebungsjahr (4-stellig)
ror96           int     %36.0g     ror96      Raumordnungsregion seit 1996
nuts2           byte    %29.0g     nuts2      NUTS-Systematik Level 2 (Regierungsbezirke etc.)

combine DE41 + DE42 - Brandenburg wurde von 2003 bis 2011 in die NUTS-2-Regionen Brandenburg-Nordost und Brandenburg-Südwest geteilt
recode DED1(Chemnitz) + DED3(Lpz) to DED4(chemnitz) + DED5(lpz)
** ------------------------------------------------------------------------ */ 
use hhnr hhnrakt  nuts2 year /// 
	if year==2011 ///
	using "${path_data}/regio/ror_l.dta", clear
mvdecode _all, mv(-3=.a \ -2=.b \ -1=.c)
rename hhnr cid
rename hhnrakt hid
rename year syear

replace nuts2 = 13 if nuts2 ==14
*label define nuts2 13 "[DE40] Brandenburg", modify
label var nuts2 "NUTS2 Region"
cap label drop nuts2
label def nuts2                          ///
        1 "[DE11] Stuttgart"         ///
        2 "[DE12] Karlsruhe"          ///
        3 "[DE13] Freiburg"         ///
        4 "[DE14] Tuebingen"          ///
		5 "[DE21] Oberbayern"         ///
        6 "[DE22] Niederbayern"          ///
		7 "[DE23] Oberpfalz"         ///
        8 "[DE24] Oberfranken"          ///
		9 "[DE25] Mittelfranken"         ///
        10 "[DE26] Unterfranken"          ///
		11 "[DE27] Schwaben"         ///
        12 "[DE30] Berlin"          ///
		13 "[DE40] Brandenburg"         ///
        14 "Old Brandenburg Süd"          ///
		15 "[DE50] Bremen"         ///
        16 "[DE60] Hamburg"          ///
		17 "[DE71] Darmstadt"         ///
        18 "[DE72] Giessen"          ///
		19 "[DE73] Kassel"         ///
        20 "[DE80] Mecklenburg-Vorpommern"         ///
        21 "[DE91] Braunschweig"         ///
        22 "[DE92] Hannover"          ///
        23 "[DE93] Lueneburg"         ///
        24 "[DE94] Weser-Ems"          ///
		25 "[DEA1] Duesseldorf"         ///
        26 "[DEA2] Koeln"          ///
		27 "[DEA3] Muenster"         ///
        28 "[DEA4] Detmold"          ///
		29 "[DEA5] Arnsberg"         ///
        30 "[DEB1] Koblenz"          ///
		31 "[DEB2] Trier"         ///
        32 "[DEB3] Rheinhessen-Pfalz"          ///
        33 "[DEC0] Saarland"         ///
        34 "[DED4] Chemnitz"          ///
		35 "[DED2] Dresden"         ///
        36 "[DED5] Leipzig"          ///
		37 "[DEE0] Sachsen-Anhalt"         ///
        38 "[DEF0] Schleswig-Holstein"          ///
		39 "[DEG0] Thueringen"         
label value nuts2 nuts2  

sort hid syear

save "${path_tmp}/hregio.tmp.dta", replace

/* ************************************************************************* * 
	PGEN
	----
	pgisced11       byte    %54.0g     pgisced11  ISCED-2011-Klassifikation
	pgfamstd        byte    %54.0g     pgfamstd   Marital Status

SOEP Values pgisced11:
                  [1] Primary education |      7,893        1.20       67.48
          [2] Lower secondary education |     28,291        4.30       71.78
          [3] Upper secondary education |    104,808       15.93       87.70
[4] Post-secondary non-tertiary educati |     18,054        2.74       90.45
     [5] Short-cycle tertiary education |      9,784        1.49       91.94
     [6] Bachelor s or equivalent level |     33,806        5.14       97.07
       [7] Master s or equivalent level |     17,334        2.63       99.71
       [8] Doctoral or equivalent level |
map to Census: 
      ISCED-Ebene 1: Primarbereich
      ISCED-Ebene 2: Sekundarbereich I
      ISCED-Ebene 3: Sekundarbereich II
      ISCED-Ebene 4: Nichttertiäre Bildung nach dem Sekundarbereich
      ISCED-Ebene 5: Erste Stufe der tertiären Bildung
      ISCED-Ebene 6: Zweite Stufe der tertiären Bildung
** ************************************************************************* */ 
	use hid pid syear ///
		 pgisced11 ///
		if syear==2011 using "${path_soep}/pgen.dta", clear

	gen educ = .
	replace educ = 1 if pgisced11 == 1
	replace educ = 2 if pgisced11 == 2
	replace educ = 3 if pgisced11 == 3
	replace educ = 4 if pgisced11 == 4 | pgisced11 == 5 //Match post-secundary and short cycle tertiary
	replace educ = 5 if pgisced11 == 6
	replace educ = 6 if pgisced11 == 7 | pgisced11 == 8 // we lose some information on Doctoral level here

	label var educ "Education ISCED"
	label define educ ///
	1 "ISCED-Ebene 1: Primarbereich" ///
	2 "ISCED-Ebene 2: Sekundarbereich I" ///
	3 "ISCED-Ebene 3: Sekundarbereich II" ///
	4 "ISCED-Ebene 4: Nichttertiäre Bildung nach dem Sekundarbereich" ///
	5 "ISCED-Ebene 5: Erste Stufe der tertiären Bildung" ///
	6 "ISCED-Ebene 6: Zweite Stufe der tertiären Bildung" 
label value educ educ
	drop pgisced11
sort hid pid syear
save "${path_tmp}/pgen.tmp.dta", replace

/* ************************************************************************* * 
	PL
	--
	plh0182         byte    %54.0g     plh0182    Lebenszufriedenheit gegenwaertig
** ************************************************************************* */ 

use hid pid syear ///
		plh0182 /// /*Lebenszufriedenheit*/
		if syear==2011 using "${path_soep}/pl.dta", clear
ren plh0182 lsat
tab lsat
drop if lsat<0
sort hid pid syear
save "${path_tmp}/pl.tmp.dta", replace

/* ************************************************************************* * 
*	MERGE EVERYTHING TOGETHER 	
** ************************************************************************* */ 

use "${path_tmp}/ppathl.tmp.dta", clear
merge 1:1 pid syear using "${path_tmp}/pl.tmp.dta"
keep if _merge==3
drop _merge
sort pid syear

merge 1:1 pid syear using "${path_tmp}/pgen.tmp.dta"
keep if _merge==3
drop _merge
sort pid syear

merge m:1 hid syear using "${path_tmp}/hbrutto.tmp.dta"
keep if _merge==3
drop _merge
sort pid syear

merge m:1 hid cid syear using "${path_tmp}/hregio.tmp.dta"
keep if _merge==3
drop _merge
sort pid syear

erase "${path_tmp}/ppathl.tmp.dta"
erase "${path_tmp}/pl.tmp.dta"
erase "${path_tmp}/pgen.tmp.dta"
erase "${path_tmp}/hbrutto.tmp.dta"
erase "${path_tmp}/hregio.tmp.dta"

compress
mvdecode _all, mv(-3=.a \ -2=.b \ -1=.c \-5 -6 -8 =.d)

local varlist bula female agegrp educ lsat nuts2
foreach var of local varlist {
	drop if `var' >=.
}

order hid pid bula nuts2 female agegrp educ lsat
saveold "${path_data}/nuts2/soep_2011_nuts2.dta", replace version(12)

/* ************************************************************************* *\ 
*	Sample sizes for ps cells
\* ************************************************************************* */ 

drop hid pid cid bula
collapse (count) lsat, by(nuts2 female agegrp educ)
rename lsat n
sort nuts2 female agegrp educ n
saveold "${path_tmp}/pstable_sample.tmp.dta", replace version(12)

// merge with census

use "${path_data}/nuts2/census_nuts2.dta", clear
sort nuts2 female agegrp educ 
merge 1:1 nuts2 female agegrp educ ///
		using "${path_tmp}/pstable_sample.tmp.dta"
recode n (. = 0)
drop _merge
saveold "${path_data}/nuts2/pstable_nuts2.dta", replace version(12)
erase "${path_tmp}/pstable_sample.tmp.dta"
exit
