/* ************************************************************************* * 
* 	Creator: Christoph
* 	Project: MRP for regional SWB index
* 	Last modified: 2019-10-14
*  ------------------------------------------------------------------------  * 
*	This file retrieves data from SOEP v33 1984-2016.
*	Cross sectional data set for 2011 (census year)
** ************************************************************************* */ 

/* ************************************************************************* * 
*	COLLECT INDIVIDUAL CHARACTERISTICS
** ************************************************************************* */ 

/* ************************************************************************* * 
	PPFADL
	------

	pid             long   %12.0g                 Unveraenderliche Personennummer
	syear           int    %8.0g                  Erhebungsjahr (SurveyYear)
	sex             byte   %53.0g      sex        Geschlecht
	gebjahr         int    %8.0g                  Geburtsjahr -4Steller-
	migback         byte   %53.0g      migback    Migrationshintergrund
	psample         byte   %53.0g      psample    Stichprobenart
	hid             long   %12.0g                 ID Haushalt
	netto           byte   %53.0g      netto      Aktueller Befragungsstatus
	nett1           byte   %53.0g      nett1      Aktueller Befragungsstatus (Netold)
	sampreg         byte   %53.0g      sampreg    Aktuelle Stichprobenregion
	pop             byte   %53.0g      pop        Aktuelle Populationszugehoerigkeit
	phrf            double %10.0g                 Hochrechnungsfaktor
	erstbefr        int    %8.0g                  Jahr der ersten Befragung
** ************************************************************************* */ 

*(fold)
use hid pid syear 														///
	gebjahr sex migback nett1 netto psample sampreg pop phrf erstbefr 	///
	if syear==2011 													///
	using "${path_soep}/ppfadl.dta", clear
sort pid syear
keep if nett1==1
keep if pop==1 | pop==2
gen age = syear - gebjahr if gebjahr>0
label var age "Age"

gen agegrp = .
replace agegrp = 1 if age>=20 & age<30
replace agegrp = 2 if age>=30 & age<40
replace agegrp = 3 if age>=40 & age<50
replace agegrp = 4 if age>=50 & age<60
replace agegrp = 5 if age>=60 & age<70
replace agegrp = 6 if age>=70 & age<80
replace agegrp = 7 if age>=80 & age<.
label var agegrp "Age group"
label def agegrp 1 "20 - 29" 2 "30 - 39" 3 "40 - 49" 4 "50 - 59"  ///
				 5 "60 - 69" 6 "70 - 79" 7 "80+", modify
label val agegrp agegrp

recode sex (1=0) (2=1)  (else=.), gen(female)
label var female "Female (0/1)"
drop sex

gen immigrant = migback!=1 if migback>0
drop migback 
ren immigrant migback

keep hid pid syear agegrp migback female phrf
save "${path_data}/ppfadl.tmp.dta", replace	
*(end)

/*	--------( pull: hbrutto )--------------------------------------------- **
	hhgr            byte   %8.0g                  Zahl der Personen im Haushalt
	bula            byte   %22.0g      bula       Bundesland
** ------------------------------------------------------------------------ */ 
*(fold)
use hid syear ///
 	bula ///
	if syear==2011 ///
	using "${path_soep}/hbrutto.dta", clear
mvdecode _all, mv(-3=.a \ -2=.b \ -1=.c)
sort hid syear
label var bula "State"
label def bula  ///
			1 "SH"       ///
			2 "HH"       ///
			3 "NI"       ///
			4 "HB"       ///
			5 "NRW"       ///
			6 "HE"       ///
			7 "RP"       ///
			8 "BW"       ///
			9 "BY"       ///
			10 "SL"     ///
			11 "BE"      ///
			12 "BB"      ///
			13 "MV"      ///
			14 "SN"      ///
			15 "ST"      ///
			16 "TH", modify
label val bula bula

save "${path_data}/hbrutto.tmp.dta", replace
*(end)


/* ************************************************************************* * 
	PGEN
	----
	
	pgpsbil         byte    %54.0g     pgpsbil    Schulabschluss
	pgfamstd        byte    %54.0g     pgfamstd   Marital Status
	pglfs
	pgnation
** ************************************************************************* */ 
*(fold)
use hid pid syear ///
    pgpsbil pgfamstd ///
    if syear==2011 using "${path_soep}/pgen.dta", clear
ren pgpsbil educ
label var educ "Education"
label def educ                          ///
        1 "Hauptschulabschluss"         ///
        2 "Realschulabschluss"          ///
        3 "Fachhochschulreife"          ///
        4 "Abitur"                      ///
        /* 5 "Anderer Abschluss"          */ ///
        6 "Ohne Abschluss verlassen"    ///
        7 "Noch kein Abschluss"
label val educ educ
drop if educ == -1 | educ == 5 // keine Entsprechung für 5 (anderer Abschluss) im Zensus

ren pgfamstd mar
drop if mar<0
recode mar (2 6 7 8 = 1) (3 = 2) (4 = 3) (5 = 4)
tab mar
label var mar "Marital status"
label def mar 1 "married" 2 "single" 3 "divorced" 4 "widowed" 5 "NA"
label val mar mar
sort hid pid syear
save "${path_data}/pgen.tmp.dta", replace
*(end)

/* ************************************************************************* * 
	PL
	--
	plh0182         byte    %54.0g     plh0182    Lebenszufriedenheit gegenwaertig
** ************************************************************************* */ 

*(fold)
use hid pid syear ///
		plh0182 /// /*Lebenszufriedenheit*/
		if syear==2011 using "${path_soep}/pl.dta", clear
ren plh0182 lsat
tab lsat
drop if lsat<0
sort hid pid syear
save "${path_data}/pl.tmp.dta", replace

/* ************************************************************************* * 
*	MERGE EVERYTHING TOGETHER 	
** ************************************************************************* */ 
*(fold)
use "${path_data}/ppfadl.tmp.dta", clear
merge 1:1 pid syear using "${path_data}/pl.tmp.dta"
keep if _merge==3
drop _merge
sort pid syear

merge 1:1 pid syear using "${path_data}/pgen.tmp.dta"
drop if _merge==2
drop _merge
sort pid syear

merge m:1 hid syear using "${path_data}/hbrutto.tmp.dta"
drop if _merge==2
drop _merge
sort pid syear

sort pid syear

erase "${path_data}/ppfadl.tmp.dta"
erase "${path_data}/pl.tmp.dta"
erase "${path_data}/pgen.tmp.dta"
erase "${path_data}/hbrutto.tmp.dta"
*(end)

compress
mvdecode _all, mv(-3=.a \ -2=.b \ -1=.c \-5 -6 -8 =.d)

local varlist bula female agegrp educ mar migback lsat
foreach var of local varlist {
	drop if `var' >=.
}

order hid pid bula female agegrp educ mar migback lsat
saveold "${path_data}/soep_2011.dta", replace version(12)

/* ************************************************************************* *\ 
*	Sample sizes for ps cells
\* ************************************************************************* */ 

drop hid pid
collapse (count) lsat, by(bula female agegrp educ mar migback)
ren lsat n
sort bula female agegrp educ mar migback n
saveold "${path_data}/pstable_sample.tmp.dta", replace version(12)

// merge with census

use "${path_data}/census2011/pstable0.dta", clear
sort bula female agegrp educ mar migback
merge 1:1 bula female agegrp educ mar migback  ///
		using "${path_data}/pstable_sample.tmp.dta"
recode n (. = 0)
drop _merge
saveold "${path_data}/census2011/pstable.dta", replace version(12)
erase "${path_data}/pstable_sample.tmp.dta"
exit









