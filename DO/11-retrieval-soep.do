/* ************************************************************************* * 
* 	Creator: Andreas
* 	Project: MRP for regional SWB index
* 	Last modified: 2020-07-16
*  ------------------------------------------------------------------------  * 
*	This file retrieves data from SOEP v34 1984-2016.
*	Cross sectional data set for 2011 (census year)
** ************************************************************************* */ 

/* ************************************************************************* * 
*	COLLECT INDIVIDUAL CHARACTERISTICS FROM PPATHL
** ************************************************************************* */ 

/* ************************************************************************* * 
	PPFADL
	------
pid             long    %12.0g                Unveraenderliche Personennummer
syear           int     %12.0g                imputation number
cid             long    %12.0g                Case ID, Ursprungshaushaltsnummer
sex             byte    %54.0g     sex        Geschlecht
gebjahr         int     %12.0g                Geburtsjahr -4Steller-
hid             long    %12.0g                ID Haushalt
nett1           byte    %54.0g     nett1      Aktueller Befragungsstatus (Netold)
pop             byte    %75.0g     pop        Aktuelle Populationszugehoerigkeit
phrf            double  %10.0g                Hochrechnungsfaktor

** ************************************************************************* */ 

use hid cid pid syear 													///
	gebjahr sex nett1 pop phrf 						///
	if syear==2000 													///
	using "${path_soep}/ppathl.dta", clear
sort pid syear
keep if nett1==1
keep if pop==1 | pop==2 // ist das notwendig?
gen age = syear - gebjahr if gebjahr>0
label var age "Age"

gen agegrp = .
replace agegrp = 1 if age>=15 & age<30
replace agegrp = 2 if age>=30 & age<45
replace agegrp = 3 if age>=45 & age<60
replace agegrp = 4 if age>=60 & age<75
replace agegrp = 5 if age>=75 & age<.
label var agegrp "Age group"

label define agegrp ///
	1 "15-29" ///
	2 "30-44" ///
	3 "45-59" ///
	4 "60-74" ///
	5 ">=75"
label value agegrp agegrp

recode sex (1=0) (2=1)  (else=.), gen(female)
label var female "Female (0/1)"
drop sex


keep hid cid pid syear agegrp female phrf
save "${path_tmp}/ppathl.tmp.dta", replace	

/* ************************************************************************* * 
*	COLLECT BULA INFORMATION FROM PPATHL
** ************************************************************************* */ 
/*	--------( pull: hbrutto )--------------------------------------------- **
	bula            byte   %22.0g      bula       Bundesland
** ------------------------------------------------------------------------ */ 
use hid syear bula ///
	if syear==2000 ///
	using "${path_soep}/hbrutto.dta", clear
mvdecode _all, mv(-3=.a \ -2=.b \ -1=.c)
sort hid syear
label var bula "State"
label def bula  ///
			1 "SH"       ///
			2 "HH"       ///
			3 "NI"       ///
			4 "HB"       ///
			5 "NRW"       ///
			6 "HE"       ///
			7 "RP"       ///
			8 "BW"       ///
			9 "BY"       ///
			10 "SL"     ///
			11 "BE"      ///
			12 "BB"      ///
			13 "MV"      ///
			14 "SN"      ///
			15 "ST"      ///
			16 "TH", modify
label val bula bula

save "${path_tmp}/hbrutto.tmp.dta", replace

/* ************************************************************************* * 
*	COLLECT REGIONAL INFORMATION FROM REGIO 2000
** ************************************************************************* */ 
/*	--------( pull: ror_l)--------------------------------------------- **
hhnr            double  %10.0g                Ursprungshaushaltsnummer
hhnrakt         double  %10.0g                Aktuelle Haushaltsnummer
qkkz            double  %10.0g		      Kreiskennziffer

** ------------------------------------------------------------------------ */ 
use hhnr hhnrakt qkkz ///
	using "${path_data}/soep_regio/soep_kkz2000.dta", clear
rename hhnr cid
rename hhnrakt hid

drop if qkkz == -2 | qkkz == -1
replace qkkz = 11000 if qkkz == 11100 | qkkz == 11200 // replace berlin ost & berlin west with Berlin

*In the Mikrozensus certain Kreise have been merged into one, as the smallest regional unit has to have 100 000 observations. 
*The following codes will be changed accordingly. Following document 09 Raumordnungsregionen from the documentation
replace qkkz = 01059 if qkkz == 01001
replace qkkz = 01058 if qkkz == 01004
replace qkkz = 13058 if qkkz == 13006
replace qkkz = 13057 if qkkz == 13005 | qkkz == 13061
replace qkkz = 13059 if qkkz == 13001 | qkkz == 13062
replace qkkz = 13055 if qkkz == 13002
replace qkkz = 13056 if qkkz == 13052
replace qkkz = 03452 if qkkz == 03402
replace qkkz = 03455 if qkkz == 03405 | qkkz == 03462
replace qkkz = 03352 if qkkz == 03461
replace qkkz = 03251 if qkkz == 03401
replace qkkz = 03354 if qkkz == 03360
replace qkkz = 03155 if qkkz == 03156 | qkkz == 03255
replace qkkz = 12068 if qkkz == 12070
replace qkkz = 12067 if qkkz == 12053
replace qkkz = 12069 if qkkz == 12051
replace qkkz = 15352 if qkkz == 15364 | qkkz == 15367
replace qkkz = 15355 if qkkz == 15362
replace qkkz = 15357 if qkkz == 15369
replace qkkz = 15154 if qkkz == 15101
replace qkkz = 15171 if qkkz == 15151
replace qkkz = 15159 if qkkz == 15153
replace qkkz = 15260 if qkkz == 15266
replace qkkz = 15261 if qkkz == 15265 | qkkz == 15268
replace qkkz = 06431 if qkkz == 06437
replace qkkz = 16062 if qkkz == 16065
replace qkkz = 16071 if qkkz == 16055
replace qkkz = 16067 if qkkz == 16068
replace qkkz = 16066 if qkkz == 16054
replace qkkz = 16063 if qkkz == 16056
replace qkkz = 16072 if qkkz == 16069
replace qkkz = 16074 if qkkz == 16053
replace qkkz = 14389 if qkkz == 14375
replace qkkz = 14284 if qkkz == 14263
replace qkkz = 14292 if qkkz == 14264
replace qkkz = 14181 if qkkz == 14171
replace qkkz = 14173 if qkkz == 14188
replace qkkz = 14178 if qkkz == 14166
replace qkkz = 07140 if qkkz == 07135
replace qkkz = 07235 if qkkz == 07211
replace qkkz = 07233 if qkkz == 07232
replace qkkz = 07133 if qkkz == 07134
replace qkkz = 07331 if qkkz == 07319
replace qkkz = 07340 if qkkz == 07320 | qkkz == 07317
replace qkkz = 07333 if qkkz == 07336
replace qkkz = 07338 if qkkz == 07311 | qkkz == 07318
replace qkkz = 07337 if qkkz == 07313
replace qkkz = 07332 if qkkz == 07316
replace qkkz = 10042 if qkkz == 10046
replace qkkz = 08216 if qkkz == 08211
replace qkkz = 09671 if qkkz == 09661
replace qkkz = 09679 if qkkz == 09675
replace qkkz = 09678 if qkkz == 09662
replace qkkz = 09674 if qkkz == 09673
replace qkkz = 09471 if qkkz == 09461
replace qkkz = 09473 if qkkz == 09463
replace qkkz = 09478 if qkkz == 09476
replace qkkz = 09472 if qkkz == 09462 | qkkz == 09477
replace qkkz = 09475 if qkkz == 09464 | qkkz == 09479
replace qkkz = 09371 if qkkz == 09361
replace qkkz = 09374 if qkkz == 09363 | qkkz == 09377
replace qkkz = 09576 if qkkz == 09565
replace qkkz = 09571 if qkkz == 09561 | qkkz == 09575 | qkkz == 09577
replace qkkz = 09779 if qkkz == 09773
replace qkkz = 09186 if qkkz == 09185
replace qkkz = 09275 if qkkz == 09262
replace qkkz = 09278 if qkkz == 09263
replace qkkz = 09272 if qkkz == 09276
replace qkkz = 09274 if qkkz == 09261
replace qkkz = 09277 if qkkz == 09279
replace qkkz = 09778 if qkkz == 09764
replace qkkz = 09777 if qkkz == 09762
replace qkkz = 09780 if qkkz == 09763 | qkkz == 09776
replace qkkz = 09173 if qkkz == 09182
replace qkkz = 09190 if qkkz == 09180
replace qkkz = 09187 if qkkz == 09163
replace qkkz = 09189 if qkkz == 09172

merge m:m qkkz using "${path_data}/kreisnamen2000.dta"
keep if _merge == 3
drop _merge

label var qkkz "Kreiskennziffer"

do "${path_data}/label_mzkr.do"
label value qkkz ef711

save "${path_tmp}/hregio.tmp.dta", replace

/* ************************************************************************* * 
	PGEN
	----
pid             long    %12.0g                Unveraenderliche Personennummer
hid             long    %12.0g                Haushaltsnummer
syear           int     %12.0g                Erhebungsjahr
pgnation        int     %54.0g     pgnation   1. Staatsangehoerigkeit
pgfamstd        byte    %82.0g     pgfamstd   Marital Status
pglfs           byte    %54.0g     pglfs      Labor Force Status
pgisced97       byte    %54.0g     pgisced97  ISCED-1997-Klassifikation

** ************************************************************************* */ 
use hid pid syear ///
	 pgisced97 pgfamstd pglfs pgnation ///
	if syear==2000 using "${path_soep}/pgen.dta", clear

gen educ = .
replace educ = 1 if pgisced97 == 1
replace educ = 2 if pgisced97 == 2
replace educ = 3 if pgisced97 == 3
replace educ = 4 if pgisced97 == 4 
replace educ = 5 if pgisced97 == 5
replace educ = 6 if pgisced97 == 6 

drop if educ >= .

label var educ "Education ISCED"
label define educ ///
1 "ISCED-Ebene 1: Primarbereich" ///
2 "ISCED-Ebene 2: Sekundarbereich I" ///
3 "ISCED-Ebene 3: Sekundarbereich II" ///
4 "ISCED-Ebene 4: Nichttertiäre Bildung nach dem Sekundarbereich" ///
5 "ISCED-Ebene 5: Erste Stufe der tertiären Bildung" ///
6 "ISCED-Ebene 6: Zweite Stufe der tertiären Bildung" 

label value educ educ
drop pgisced97

gen maritalstatus = .
replace maritalstatus = 2 if pgfamstd == 1 | pgfamstd == 2 | pgfamstd == 6
replace maritalstatus = 1 if pgfamstd == 3
replace maritalstatus = 3 if pgfamstd == 5
replace maritalstatus = 4 if pgfamstd == 4

label var maritalstatus "Marital status"
label define maritalstatus ///
	1 "Ledig" ///
	2 "Verheiratet" ///
	3 "Verwitwet" ///
	4 "Geschieden" 
label value maritalstatus maritalstatus	
drop if maritalstatus >= .
drop pgfamstd

gen german = .
replace german = 1 if pgnation == 1
replace german = 0 if pgnation != 1

label var german "German Citizenship (0/1)"
drop pgnation

gen job = .
replace job = 1 if pglfs == 11 | pglfs == 12
replace job = 2 if pglfs == 6
replace job = 3 if pglfs == 1 | pglfs == 2 | pglfs == 3 | pglfs == 4 | pglfs == 5 | pglfs == 8 | pglfs == 9 | pglfs == 10

label var job "Occupational Status"
label define job ///
	1 "Employed" ///
	2 "Unemployed" ///
	3 "Inactive"
label value job job 
drop pglfs

sort hid pid syear
save "${path_tmp}/pgen.tmp.dta", replace

/* ************************************************************************* * 
	PL
	--
	plh0182         byte    %54.0g     plh0182    Lebenszufriedenheit gegenwaertig
** ************************************************************************* */ 

use hid pid syear ///
		plh0182 /// /*Lebenszufriedenheit*/
		if syear==2000 using "${path_soep}/pl.dta", clear
ren plh0182 lsat
tab lsat
drop if lsat<0
sort hid pid syear
save "${path_tmp}/pl.tmp.dta", replace

/* ************************************************************************* * 
*	MERGE EVERYTHING TOGETHER 	
** ************************************************************************* */ 

use "${path_tmp}/ppathl.tmp.dta", clear
merge 1:1 pid syear using "${path_tmp}/pl.tmp.dta"
keep if _merge==3
drop _merge
sort pid syear

merge 1:1 pid syear using "${path_tmp}/pgen.tmp.dta"
keep if _merge==3
drop _merge
sort pid syear

merge m:1 hid syear using "${path_tmp}/hbrutto.tmp.dta"
keep if _merge==3
drop _merge
sort pid syear

merge m:1 hid cid using "${path_tmp}/hregio.tmp.dta"
keep if _merge==3
drop _merge
sort pid syear

erase "${path_tmp}/ppathl.tmp.dta"
erase "${path_tmp}/pl.tmp.dta"
erase "${path_tmp}/pgen.tmp.dta"
erase "${path_tmp}/hbrutto.tmp.dta"
erase "${path_tmp}/hregio.tmp.dta"

compress
mvdecode _all, mv(-3=.a \ -2=.b \ -1=.c \-5 -6 -8 =.d)

local varlist female agegrp educ maritalstatus german job lsat bula qkkz 
foreach var of local varlist {
	drop if `var' >=.
}

drop phrf syear

/* ************************************************************************* *\ 
*	Add regional data to the observations
\* ************************************************************************* */ 

merge m:1 qkkz using "${path_data}/kkz/kkz_unemployment_2000.dta"

keep if _merge == 3
drop _merge

merge m:1 qkkz using "${path_data}/kkz/kkz_income_2000.dta"
keep if _merge == 3
drop _merge raumeinheit kreisname Kreisname

order cid hid pid qkkz female agegrp educ maritalstatus job german lsat bula income unemployment
saveold "${path_data}/soep_2000_qkkz.dta", replace version(12)

/* ************************************************************************* *\ 
*	Sample sizes for ps cells
\* ************************************************************************* */ 

drop hid pid cid bula
collapse (count) lsat, by(qkkz female agegrp educ maritalstatus job german) // 
rename lsat n
sort qkkz female agegrp educ maritalstatus job german n // 
saveold "${path_tmp}/pstable_sample.tmp.dta", replace version(12)



use "${path_data}/pstable_mz_regio.dta", clear
rename mzkr qkkz
sort qkkz female agegrp educ maritalstatus job german // 
merge 1:1 qkkz female agegrp educ maritalstatus job german  ///
		using "${path_tmp}/pstable_sample.tmp.dta" //
recode n (. = 0) if _merge == 1
drop if _merge == 2 //No corresponding information for these SOEP-cells in Mikrozensus (Mostly 1 person per cell, rare categories (widowed, divorced etc)) we lose about 2000 SOEP observations here
drop _merge
saveold "${path_data}/pstable_kkz.dta", replace version(12)
erase "${path_tmp}/pstable_sample.tmp.dta"
exit
