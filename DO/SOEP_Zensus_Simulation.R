#MRP with rstanarm Tutorial Lauren Kennedy
Sys.setenv(LANG = "en")
rm(list=ls())
#Load packages ####

library(truncnorm)
library(rstanarm)
library(ggplot2)
library(bayesplot)
library(dplyr)
library(tidyr)
library(brms)
library(fabricatr)
library(parallel)
theme_set(bayesplot::theme_default())
options(mc.cores = detectCores()) 

#Define function that simulates mrp data ####
simulate_mrp_data <- function(n) {
  J <- c(2, 3, 5, 3, 16) # male or not, Erwerbst?tigkeit (Erwerbst?tig, Erwerbslos, Nichterwerbsperson) , age (<18, 18-29, 30-49, 50-64, >64), Migrationshintergrund (ohne Migback, Miback, Ausl?nder), Raumordnungsregion
  poststrat <- as.data.frame(array(NA, c(prod(J), length(J)+1))) # Columns of post-strat matrix, plus one for size
  colnames(poststrat) <- c("male", "job", "age","migback", "state",'N')
  count <- 0
  for (i1 in 1:J[1]){ # for i1 in 1:2 (i.e. for both genders)
    for (i2 in 1:J[2]){
      for (i3 in 1:J[3]){
        for (i4 in 1:J[4]){
          for (i5 in 1:J[5]){ # 1:J[5] is 1:50 because J[5] is 50
            count <- count + 1
            # Fill them in so we know what category we are referring to
            poststrat[count, 1:5] <- c(i1-1, i2, i3,i4,i5) 
          }
        }
      }
    }
  }
  # Proportion in each sample in the population (Values based on Zensus 2011 DE)
  p_male <- c(0.512, 0.488)
  p_job <- c(0.515, 0.025, 0.459)
  p_age <- c(0.164,0.142,0.285,0.204,0.206)
  p_migback <- c(.808,.116,.76)
  p_state_tmp <- runif(n = 16, min = 10,max = 20)
  p_state <- p_state_tmp/sum(p_state_tmp)
  poststrat$N <- 0
  for (j in 1:prod(J)){ # 82e6 is a random massive population size here, about Germany
    poststrat$N[j] <- round(82e6 * p_male[poststrat[j,1]+1] * # get the value for each j in poststrat and multiply by the proportions in the sample
                      p_job[poststrat[j,2]] * p_age[poststrat[j,3]] * 
                      p_migback[poststrat[j,4]] * p_state[poststrat[j,5]]) #Adjust the N to be the number observed in each category in each group
  }
  
  # Now let's adjust for the probability of response 
  p_response_baseline <- 0.01
  p_response_male <- c(2, 0.8) / 2.8
  p_response_job <- c(1, 1.2, 2.5) / 4.7
  p_response_age <- c(0.5, 0.4, 1, 1.5,  3) / 6.4
  p_response_migback <- c(1, 0.8, 0.6) / 2.4
  p_response_state <- rbeta(n = 16, shape1 =  1, shape2 =  1)
  p_response_state <- p_response_state / sum(p_response_state)
  p_response <- rep(NA, prod(J))
  for (j in 1:prod(J)) {
    p_response[j] <-
      p_response_baseline * p_response_male[poststrat[j, 1] + 1] *
      p_response_job[poststrat[j, 2]] * p_response_age[poststrat[j, 3]] *
      p_response_migback[poststrat[j, 4]] * p_response_state[poststrat[j, 5]]
  }
  people <- sample( prod(J), size = n, replace = TRUE, prob = poststrat$N * p_response) #sample random people depending on the proportions and response rate of that cell
  
  ## For respondent i, people[i] is that person's poststrat cell, (Which cell does that person in our sample stem from)
  ## some number between 1 and 32
  n_cell <- rep(NA, prod(J))
  for (j in 1:prod(J)) {
    n_cell[j] <- sum(people == j)
  }
  
  coef_male <- c(0, -1)
  coef_job <- c(1, -1, 0.5)
  coef_age <- c(2, 0 , -1, -0.5, 1)
  coef_migback <- c(0, -0.5, -1)
  coef_state <- c(0, round(rnorm(15, 0, 1.5), 1))

  true_popn <- data.frame(poststrat[, 1:5], lsat = rep(NA, prod(J))) # get the first columns from the cell matrix, define a new one with variable of interest NAs
  for (j in 1:prod(J)) {
    true_popn$lsat_influence[j] <- sum(  # life satisfaction for each subgroup as truncated normal distributed variable
      coef_male[poststrat[j, 1] + 1] +
        coef_job[poststrat[j, 2]] + coef_age[poststrat[j, 3]] +
        coef_migback[poststrat[j, 4]] + coef_state[poststrat[j, 5]]
        )
  }
    true_popn$lsat <- rtruncnorm(n = 1, a = 0, b = 10, sd = 2, mean = 6 + true_popn$lsat_influence)
  #male or not, jobstatus, age, migback, state
  y <- round(true_popn$lsat[people] , 0) #for our sample "people" we get the voting preference from the cell the person belongs to
  male <- poststrat[people, 1]
  job <- poststrat[people, 2]
  age <- poststrat[people, 3]
  migback <- poststrat[people, 4]
  state <- poststrat[people, 5]
  
  sample <- data.frame(lsat = y, 
                       male, age, job, migback, state, 
                       id = 1:length(people)) #create a dataframe from our people sample
  
  #Make all numeric:
  for (i in 1:ncol(poststrat)) {
    poststrat[, i] <- as.numeric(poststrat[, i])
  }
  for (i in 1:ncol(true_popn)) {
    true_popn[, i] <- as.numeric(true_popn[, i])
  }
  for (i in 1:ncol(sample)) {
    sample[, i] <- as.numeric(sample[, i])
  }
  list(
    sample = sample,
    poststrat = poststrat,
    true_popn = true_popn
  )
}
# Generate sample data using the function ####

mrp_sim <- simulate_mrp_data(n=5000)
str(mrp_sim)
sample <- mrp_sim[["sample"]]
rbind(head(sample), tail(sample)) # Rowbinds the first and last rows together and prints them for an overview

poststrat <- mrp_sim[["poststrat"]]
rbind(head(poststrat), tail(poststrat))

true_popn <- mrp_sim[["true_popn"]]
rbind(head(true_popn), tail(true_popn))

#Make state a factor and order it by occurence in the sampel
sample$state <- factor(sample$state, levels=1:16)
sample$state <- with(sample, factor(state, levels=order(table(state))))
true_popn$state <- factor(true_popn$state,levels = levels(sample$state))
poststrat$state <- factor(poststrat$state,levels = levels(sample$state))

#Fit the model to our sample ####
fit <- stan_glmer(
  cat_pref ~ factor(male) + factor(male) * factor(age) + 
    (1 | state) + (1 | age) + (1 | eth) + (1 | income),
  family = binomial(link = "logit"),
  data = sample, chains=8, cores=8, control=list(adapt_delta=.99), 
)

fit2 <- brm(lsat | trunc(lb=0, ub = 10) ~ (1|male)  + (1|age) + (1|job) + (1|migback) + (1|state), data=sample, chains=8, cores=8, control=list(adapt_delta=.999, max_treedepth=20), iter = 4000, family = gaussian())
print(fit2, detail=T)
get_prior(sat | trunc(lb=0, ub = 10) ~ factor(male) + factor(male) * factor(age) + (1|age) + (1|job) + (1|migback) + (1|state), data=sample, family = gaussian())
loo(fit1, fit2, fit3)

fit3 <- brm(lsat | trunc(lb = 0 , ub = 10) ~ (1 + male + age + job + migback | state ),
            data=sample, chains=8, cores=8, 
            control=list(adapt_delta=.999, max_treedepth=20), 
            iter = 4000, warmup = 2000 ,family = gaussian()  )
loo(fit1, fit2, fit3)
print(fit3, detail=T, WAIC=T)
ranef(fit1)
coef(fit2)

#Get the population estimate #####
posterior_prob <- posterior_linpred(object = fit3, transform = T, newdata = poststrat) # draw predictions from the posterior for all the cells 
posterior_prob <- pp_expect(object = fit1, newdata = poststrat)
poststrat_prob <- (posterior_prob %*% poststrat$N) / sum(poststrat$N) #Multiply the probability by the number of people in the population in each Cell and divide by general pop
model_popn_pref <- c(mean = mean(poststrat_prob), sd = sd(poststrat_prob))
round(model_popn_pref, 3)

#Compare to simple average values using the sample
sample_popn_pref <- mean(sample$lsat)
round(sample_popn_pref, 3)

#Compare to the true value in the simulated data
true_popn_pref <- sum(true_popn$lsat * poststrat$N) / sum(poststrat$N)
round(true_popn_pref, 3)

#Get estimates for states
state_df <- data.frame(
  State = 1:16,
  model_state_sd = rep(-1, 16),
  model_state_pref = rep(-1, 16),
  sample_state_pref = rep(-1, 16),
  true_state_pref = rep(-1, 16),
  N = rep(-1, 16)
) #create dataframe that will hold predictions for states

for(i in 1:length(levels(as.factor(poststrat$state)))) {
  poststrat_state <- poststrat[poststrat$state == i, ] #get the cells for a specific state
  posterior_prob_state <- pp_expect( #get predictions for each cell in the state
    object = fit1,
    draws = 1000,
    newdata = as.data.frame(poststrat_state)
  )
  poststrat_prob_state <- (posterior_prob_state %*% poststrat_state$N) / sum(poststrat_state$N) # Calculate the population values for that state
  #This is the estimate for popn in state:
  state_df$model_state_pref[i] <- round(mean(poststrat_prob_state), 4)
  state_df$model_state_sd[i] <- round(sd(poststrat_prob_state), 4)
  #This is the estimate for sample
  state_df$sample_state_pref[i] <- round(mean(sample$lsat[sample$state == i]), 4)
  #And what is the actual popn?
  state_df$true_state_pref[i] <-
    round(sum(true_popn$lsat[true_popn$state == i] * poststrat_state$N) /
            sum(poststrat_state$N), digits = 4)
  state_df$N[i] <- length(sample$lsat[sample$state == i])
}

state_df[c(1,3:6)]
state_df$State <- factor(state_df$State, levels = levels(sample$state))

round(c(
  mean = mean(abs(state_df$sample_state_pref-state_df$true_state_pref), na.rm = TRUE),
  max = max(abs(state_df$sample_state_pref-state_df$true_state_pref), na.rm = TRUE)
), 2)
state_df$diff_sample <- abs(state_df$sample_state_pref-state_df$true_state_pref)
state_df$diff_model <- abs(state_df$model_state_pref-state_df$true_state_pref)
round(c(
  mean = mean(abs(state_df$model_state_pref-state_df$true_state_pref)),
  max = max(abs(state_df$model_state_pref-state_df$true_state_pref))
),2)

#Alternative Simulation for individual data using fabricatr####



#Alternative modelling ####
# not evaluated to avoid dependency on tidyverse
sample_alt <- sample %>%
  group_by(male, age, income, state, eth) %>%
  summarise(N_cat_pref = sum(cat_pref), N = n()) %>%
  ungroup()

fit2 <- stan_glmer(
  cbind(N_cat_pref, N - N_cat_pref) ~ factor(male) + factor(male) * factor(age) + 
    (1 | state) + (1 | age) + (1 | eth) + (1 | income),
  family = binomial("logit"),
  data = sample_alt,
  refresh = 0
)
print(fit2)

posterior_prob_alt <- posterior_linpred(fit2, transform = TRUE, newdata = poststrat)
poststrat_prob_alt <- posterior_prob_alt %*% poststrat$N / sum(poststrat$N)
model_popn_pref_alt <- c(mean = mean(poststrat_prob_alt), sd = sd(poststrat_prob_alt))
round(model_popn_pref_alt, 3)
