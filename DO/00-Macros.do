/* ************************************************************************* * 
 *   Creator: Andreas
 *   Project: How is life in Germany?
 *   Last modified: 2020-05-09
 *   ------------------------------------------------------------------------
 *   This file defines global macros.
** ************************************************************************* */ 

/* ************************************************************************* *\ 
 *   Define macros / paths
\* ************************************************************************* */ 
* Christoph		
global homedir 		"~/Documents/swb-index"
global path_soep    "$homedir/data/soep_v34/soep.v34/stata_de+en"
global path_soepX   "~/Documents/Uni/Data/soep_v33/SOEP-CORE_v33.1_stata_bilingual"
global path_data    "$homedir/data"
global path_prgs    "$homedir/do"
global path_tmp	    "$homedir/temp"
global path_logs    "$homedir/log"
* global path_results "~/Documents/Uni/Projekte/Working_Hours_and_Health/results"
* global path_logs    "~/Documents/Uni/Projekte/Working_Hours_and_Health/logs/2018-03-03"
*/
