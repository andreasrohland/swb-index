/* ************************************************************************* * 
* 	Creator: Andreas Rohland
* 	Project: MRP for regional SWB index
* 	Last modified: 2020-07-15
*  ------------------------------------------------------------------------  * 
*	This file creates the poststratification table from census data (CENSUS HUB) https://ec.europa.eu/CensusHub2/intermediate.do?&method=forwardResult.
*	Cross sectional data set for 2011 (census year)
** ************************************************************************* */ 
clear all
set more off
set mem 500m

use "${path_data}/SUF MZ-Regionalfile 2000/STATA/mz00_regionalfile.dta"

keep ef1 ef30 ef32 ef35 ef52 ef70 ef71 ef72 ef286 ef287 ef288 ef289 ef504 ef539 ef710 ef711 ef752 ef804
rename (ef1 ef30 ef32 ef35 ef52 ef287 ef289 ef504 ef539 ef710 ef711 ef752 ef804) (bula age gender maritalstatus citizenship schooldegree jobdegree occupational_status income anpassungsschicht mzkr weight_mzkr ror)

drop if age <= 14 //Under 15 year olds, don't appear in SOEP

*Generate 5 age categories 
gen agegrp = .
replace agegrp = 1 if age>=15 & age<30
replace agegrp = 2 if age>=30 & age<45
replace agegrp = 3 if age>=45 & age<60
replace agegrp = 4 if age>=60 & age<75
replace agegrp = 5 if age>=75 & age<.
label var agegrp "Age group"

label define agegrp ///
	1 "15-29" ///
	2 "30-44" ///
	3 "45-59" ///
	4 "60-74" ///
	5 ">=75"
label value agegrp agegrp

* Generate female dummy
gen female = 0
replace female = 1 if gender == 2
label var female "Female"

* STATA-Job zur Umsetzung der Bildungsklassifikation ISCED-1997 mit dem Mikrozensus 2000
* ISCED-Version des German Microdata Lab, GESIS    

* Bei Nutzung dieser Routine bitte wie folgt zitieren:
* (hier wird auch die Skalenkonstruktion beschrieben)                                
* Schroedter, J. H.; Lechert, Y.; Lüttinger, P. (2006): Die Umsetzung der Bildungsskala 
*    ISCED-1997 für die Volkszählung 1970, die Mikrozensus-Zusatzerhebung 1971 und die 
*    Mikrozensen 1976-2004. ZUMA-Methodenbericht 2006/08.
* http://www.gesis.org/dienstleistungen/tools-standards/mikrodaten-tools/isced/

*******************************************************************************************.
*	Bildung der Variable "Allgemein bildender Schulabschluss" .
*	Klasse 5-10 zu Haupt-/Volksschule .
*	Klasse 11-13 zu Fachhochschulreife/Abitur .
*******************************************************************************************.

recode schooldegree 0=0 1=4 2 3=5 4 5=6 9=3, generate(as)
replace as=1 if (ef286==8)
replace as=2 if (ef286==9)
replace as=7 if (ef71==1 & ef72==2) 
replace as=8 if (ef71==1 & ef72==3) 
recode as 4 7=4 6 8=6
label variable as "Allgemein bildender Schulabschluss"
#delimit ;
label define as1 
	0 "entf."
	1 "kein SA"
	2 "o.A. ob SA"
	3 "o.A. zur Art d. SA"
	4 "HS/VS/Kl.5-10"
	5 "RS/POS" 
	6 "FHR/ABI/Kl.11-13";
#delimit cr
label values as as1    

*******************************************************************************************.
*	Bildung der Variable "Berufsbildender Abschluss" .
*******************************************************************************************.

recode jobdegree 0=0 1 2=4 3 4=5 5 6=6 7 8=7 9=8 99=3, generate(ba)
replace ba=1 if ef288==8
replace ba=2 if ef288==9
label variable ba "Beruflicher Abschluss"
#delimit ; 
label define ba1
	0 "Entfällt (Pers<15)"
	1 "kein BA"
	2 "o.A. ob BA"
	3 "o.A. zur Art d. BA"
	4 "Anlernausb./Praktikum/BVJ"
	5 "Lehrausb./Berufsfachschule"
	6 "Meister/Technik./Fachschule/Verw.FH"
	7 "FH/Hochschule"
	8 "Promotion";
#delimit cr
label values ba ba1 	

*******************************************************************************************.
*	Bildung der Hilfsvariable "Gegenwärtiger Besuch berufliche Schule" .
*******************************************************************************************.

recode ef72 4=5 5 6=7, generate(ba2), if ef71==1 & ef72>=4
recode ba2 9=.
label variable ba2 "Gegenwärtiger Besuch berufliche Schule"
#delimit ;
label define ba21
	5 "Berufliche Schule"
	7 "FH/Hochschule";
#delimit cr
label values ba2 ba21
* Anmerkung: die berufliche Schule wird hier, da nicht näher spezifiziert ***.
* zu der Lehrausbildung gezählt. ***.

*******************************************************************************************.
*	Modifikation der Variable "Berufsbildender Abschluss" .
*	Wenn der angestrebte berufliche Abschluss höher ist als der gegenwärtige, .
*	ersetzt dieser den "eigentlichen" beruflichen Abschluss .
*******************************************************************************************.

recode ba 1 2 3 4=5 if ba2==5 & ba2>ba
recode ba 1 2 3 4 5 6=7 if ba2==7 & ba2>ba

tab as ba, row col
*******************************************************************************************.
*	Bildung der Variable "ISCED-1997 - GML" .
*******************************************************************************************.

generate is=99
replace is=0 if (ef70==1 & age>= 3 & ef71~=1)
replace is=1 if (as==1 & (ba==1 | ba==2)) | (as==2 & ba==1)
replace is=2 if (ef71==1 & ef72==1)
replace is=3 if ((as==1 | as==2) & (ba==3 | ba==4)) | ((as==3 | as==4) /*
*/ & (ba==0 | ba==1 | ba==2 | ba==3 | ba==4))
replace is=4 if (as==5 & (ba==1 | ba==2 | ba==3 | ba==4))
replace is=5 if ((as==1 | as==2 | as==3 | as==4 | as==5) & ba==5)
replace is=6 if (as==6 & (ba==1 | ba==2 | ba==3 | ba==4))
replace is=7 if (as==6 & ba==5)
replace is=8 if (ba==6)
replace is=9 if (ba==7)
replace is=10 if (ba==8)

label variable is "ISCED-1997 - GML"
#delimit ;
label define is1
	0 "0"
	1 "1B"
	2 "1A"
	3 "2B"
	4 "2A"
	5 "3B"
	6 "3A"
	7 "4A"
	8 "5B"
	9 "5A"
	10 "6"
	99 "entf./nicht zuordenbar";
#delimit cr
label values is is1

tab is
drop if is == 99
/*Generate 6 Educ categories following ISCED-97 classification similar to SOEP
      ISCED-Ebene 1: Primarbereich
      ISCED-Ebene 2: Sekundarbereich I
      ISCED-Ebene 3: Sekundarbereich II
      ISCED-Ebene 4: Nichttertiäre Bildung nach dem Sekundarbereich
      ISCED-Ebene 5: Erste Stufe der tertiären Bildung
      ISCED-Ebene 6: Zweite Stufe der tertiären Bildung
*/
gen educ = .
replace educ = 1 if is == 1 | is ==2
replace educ = 2 if is == 3 | is == 4
replace educ = 3 if is == 5 | is == 6
replace educ = 4 if is == 7
replace educ = 5 if is == 8 
replace educ = 6 if is == 10 | is == 9 // 5A is merged into 6 in SOEP

label var educ "Education ISCED-97"
label define educ ///
	1 "ISCED-Ebene 1: Primarbereich" ///
	2 "ISCED-Ebene 2: Sekundarbereich I" ///
	3 "ISCED-Ebene 3: Sekundarbereich II" ///
	4 "ISCED-Ebene 4: Nichttertiäre Bildung nach dem Sekundarbereich" ///
	5 "ISCED-Ebene 5: Erste Stufe der tertiären Bildung" ///
	6 "ISCED-Ebene 6: Zweite Stufe der tertiären Bildung" 
label value educ educ


*Generate Unemployment categories
gen job = .
replace job = 1 if occupational_status == 1
replace job = 2 if occupational_status == 2 | occupational_status == 3
replace job = 3 if occupational_status == 4

label var job "Occupational Status"
label define job ///
	1 "Employed" ///
	2 "Unemployed" ///
	3 "Inactive"
label value job job 

*Generate migration background variable
gen german = 0
replace german = 1 if citizenship == 1

label var german "German Citizenship (0/1)"

*Create labels for maritalstatus equal to those in SOEP
label var maritalstatus "Marital status"
label define maritalstatus ///
	1 "Ledig" ///
	2 "Verheiratet" ///
	3 "Verwitwet" ///
	4 "Geschieden" 
label value maritalstatus maritalstatus	

* Save the labels for MZKR in a do file
label save ef711 using "${path_data}/label_mzkr.do", replace

drop age gender ef70 ef71 ef72 ef286 schooldegree ef288 jobdegree income ror as ba ba2 is occupational_status citizenship

*****************************************************
*Weight the data by MZKR

/* ************************************************************************* *\ 
*	Get weighted population values for ps cells
\* ************************************************************************* */ 

collapse (sum) weight_mzkr, by(female agegrp educ maritalstatus job german mzkr) //
rename weight_mzkr N
replace N = round(N,1)
order mzkr female agegrp educ maritalstatus job german N //
sort mzkr female agegrp educ maritalstatus job german  N //
saveold "${path_data}/pstable_mz_regio.dta", replace version(12)

exit
