# List of Abbreviations {-}

\begin{tabular}{rp{0.2cm}lp{1cm}rp{0.2cm}l}
    NUTS     & &  No-U-Turn Sampler   & & SOEP     & &  Socio-Economic Panel  \\
    MrP     & &  Multilevel regression and Poststratification   & & SWB     & &  Subjective well-being \\
    MCMC     & &  Markov Chain Monte Carlo   & & HMC     & &  Hamiltonian Monte Carlo  \\
    GDP     & &  Gross Domestic Product   & & HMC     & &  Hamiltonian Monte Carlo
\end{tabular}
